# Proyecto Final "Seguridad Criptográfica en los Sistemas Embebidos" 🚀
* Universidad Nacional Autónoma de México
* Fundamentos de Sistemas Embebidos — Semestre 2023-2

## Descripción 
* Lengaje de programación: Java ☕

## Autores ✒️
* **Nava Rosales Juan Manuel** 💻  - [manuuelnrs@GitLab](https://gitlab.com/manuuelnrs)
* **Romo Gonzalez Romo** 💻 - 